package br.unifor.cg.ex1

import br.unifor.cg.ex1.canvas.Canvas
import br.unifor.cg.ex1.canvas.CanvasFactory
import br.unifor.cg.ex1.circle.Circle
import br.unifor.cg.ex1.circle.CircleMP
import br.unifor.cg.ex1.line.Line
import br.unifor.cg.ex1.line.LineBresenham
import br.unifor.cg.ex1.line.LineXiaolinWu

import static br.unifor.cg.ex1.canvas.CanvasFactory.CanvasType.GUI

/**
 * Created by Demétrio on 09/09/2014.
 */
class AntiAliasMain {
    static def main(args) {
        int width = 500, height = 500

        Canvas canvas = CanvasFactory.getCanvas(GUI, width, height)
        canvas.draw ([[10,2],[300,200]] as LineXiaolinWu);
        canvas.draw ([[200,200],[300,150]] as LineXiaolinWu);
        //canvas.draw ([[0,20],[200,230]] as LineBresenham);
        canvas.show();
    }
}
