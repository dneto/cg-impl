package br.unifor.cg.ex1

import br.unifor.cg.ex1.canvas.Canvas
import br.unifor.cg.ex1.canvas.CanvasFactory
import br.unifor.cg.ex1.circle.Circle
import br.unifor.cg.ex1.circle.CircleMP
import br.unifor.cg.ex1.line.Line
import br.unifor.cg.ex1.line.LineBresenham
import br.unifor.cg.ex1.line.LineFactory
import br.unifor.cg.ex1.line.LineXiaolinWu

import static br.unifor.cg.ex1.canvas.CanvasFactory.CanvasType.*

class CirculoMain {
    static def main(args) {
        int width = 500, height = 500
        Circle circle = [radius: 150, origin: [250, 250]] as CircleMP
        Line line = new LineXiaolinWu();
        line.initialPoint = new Point(0,0);
        line.finalPoint = new Point(200,200);


        Line line2 = new LineXiaolinWu();
        line2.initialPoint = new Point(200,200);
        line2.finalPoint = new Point(250,150);

        Canvas canvas = CanvasFactory.getCanvas(GUI, width, height)
        canvas.draw circle
        canvas.draw line;
        canvas.draw line2;
        canvas.show();
    }
}
