package br.unifor.cg.ex1

import br.unifor.cg.ex1.line.Line
import br.unifor.cg.ex1.line.LineBruteForce

/**
 * Created by demetrio on 13/08/14.
 */
class Main {

    static def printLine(l) {
        if (l != null)
            l.each { p ->
                println "x = ${Math.round(p.x)} (${p.x}), y = ${Math.round(p.y)}(${p.y})"
            }
    }

    static def main(args) {
        Point pi1 = new Point(0, 0)
        Point pf1 = new Point(0, 10)

        Point pi2 = new Point(0, 0)
        Point pf2 = new Point(10, 0)

        //println "**METODO BRUTE FORCE***"
        Line l1 = new LineBruteForce(pi1, pf1)
        def points1 = l1.calculateLine();
        printLine points1

        Line l2 = new LineBruteForce(pi2, pf2)
        def points2 = l2.calculateLine()
        printLine points2

        String[][] m = new String[11][11];
        m.each { i ->
            i.each { j ->
                j = ''
            }
        }

        println m.length
        println m[0].length

        points1.each { p ->
            def x = (int)Math.round(p.x)
            def y = (int)Math.round(p.y)
            m[y][x] = p.color;
        }

        points2.each { p ->
            def x = (int)Math.round(p.x)
            def y = (int)Math.round(p.y)
            m[y][x] = p.color;
        }

        m.each { i ->
            i.each { j ->
                print j==null?'':j
            }
            println ''
        }

//        println "**METODO DDA***"
//        Line l = new LineDDA(pi, pf)
//        printLine l.calculateLine()


    }
}
