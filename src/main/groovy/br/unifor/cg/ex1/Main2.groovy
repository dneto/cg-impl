package br.unifor.cg.ex1

import br.unifor.cg.ex1.canvas.Canvas
import br.unifor.cg.ex1.canvas.ConsoleCanvas
import br.unifor.cg.ex1.circle.Circle
import br.unifor.cg.ex1.circle.CircleMP
import br.unifor.cg.ex1.line.LineUtils

/**
 * Created by demetrio on 14/08/14.
 */
class Main2 {


    static def main(args){
        Canvas c = new ConsoleCanvas(50,50);
        def hrL = LineUtils.horizontalLine(0,0,49);
        def vrL = LineUtils.verticalLine(0,0,49);

        c.drawPoints(hrL);
        c.drawPoints(vrL);

//        Line l = new LineBresenham(
//                new Point(5,8),
//                new Point(9,11)
//        );

//        Line l = new LineBresenham(
//                new Point(3,10),//10,7
//                new Point(9,7)//5,12
//        );

//        Line l = new LineBresenham(
//                new Point(10,7),
//                new Point(5,12)
//        );

      //  c.drawPoints(l.calculateLine());
        Circle circle = new CircleMP(4, new Point(20,20));
        c.drawPoints(circle.generateCirclePoints())

//        Circle circle2 = new CircleMP(1, new Point(20,20));
//        c.drawPoints(circle2.generateCirclePoints())
        println c;
    }
}
