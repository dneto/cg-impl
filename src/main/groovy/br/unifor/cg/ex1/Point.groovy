package br.unifor.cg.ex1

import groovy.transform.ToString

import java.awt.Color

@ToString
class Point implements Comparable<Point> {
    int x, y
    def color

    def Point(x, y) {
        this.x = x
        this.y = y
    }


    def Point(x, y, Color color) {
        this.x = x
        this.y = y
        this.color = color
    }

    @Override
    int compareTo(Point o) {
        Integer c = x + y;
        Integer oC = o.x + o.y;
        return c.compareTo(oC);
    }
}
