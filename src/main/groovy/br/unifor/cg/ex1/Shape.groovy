package br.unifor.cg.ex1

/**
 * Created by demetrio on 27/08/14.
 */
interface Shape {

    Point[] getPoints();

}
