package br.unifor.cg.ex1.canvas

import br.unifor.cg.ex1.Point
import br.unifor.cg.ex1.Shape

trait Canvas {

    int width, height

    abstract void show();

    abstract Canvas drawPoint(Point p);

    Canvas draw(Shape s){
        drawPoints(s.points)
    }

    Canvas drawPoints(ps){
        ps.each{
            drawPoint(it as Point)
        }
        return this
    }
}
