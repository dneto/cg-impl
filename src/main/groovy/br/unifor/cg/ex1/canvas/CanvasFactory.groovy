package br.unifor.cg.ex1.canvas

class CanvasFactory {

    CanvasFactory(){}

    static Canvas getCanvas(CanvasType canvasType, int width, int height){
        Canvas canvas;

        switch(canvasType){
            case CanvasType.CONSOLE:
                canvas = new ConsoleCanvas(width, height);
                break;
            case CanvasType.GUI:
                canvas = new JPanelCanvas(width,height)
                break;
        }

        return canvas;
    }

    static enum CanvasType{
        CONSOLE,
        GUI
    }

}
