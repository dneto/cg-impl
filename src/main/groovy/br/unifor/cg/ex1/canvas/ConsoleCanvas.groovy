package br.unifor.cg.ex1.canvas

import br.unifor.cg.ex1.Point

class ConsoleCanvas implements Canvas {


    private String[][] canvas

    def ConsoleCanvas(width, height) {
        this.width = width - 1
        this.height = height - 1
        canvas = new String[width][height]
    }

    @Override
    void show() {
        System.out.println(this.toString());
    }

    @Override
    Canvas drawPoint(Point p) {
        canvas[-p.y][p.x] = p.color
        return this
    }

    @Override
    String toString() {
        StringBuffer sb = new StringBuffer();

        canvas.each { i ->
            i.each { j ->
                sb.append "${j ? j : ' '}"
            }
            sb.append "\n"
        }

        return sb.toString()
    }
}
