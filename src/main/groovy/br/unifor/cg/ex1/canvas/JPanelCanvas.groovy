package br.unifor.cg.ex1.canvas

import br.unifor.cg.ex1.Point

import javax.swing.JFrame
import javax.swing.JPanel
import java.awt.Color
import java.awt.Graphics

import static javax.swing.JFrame.EXIT_ON_CLOSE

class JPanelCanvas extends JPanel implements Canvas {

    List<Point> points;

    JPanelCanvas(width, height) {
        this.width = width
        this.height = height
        this.points = []
    }

    @Override
    protected void paintComponent(Graphics g) {
        points.each {
            g.drawRect it.x, it.y, 0, 0;
            g.color = it.color
        }
    }

    @Override
    void show() {
        [size                 : [this.width, this.height],
         contentPane          : this,
         defaultCloseOperation: EXIT_ON_CLOSE,
         visible              : true,
         background           : Color.WHITE
        ] as JFrame
    }

    @Override
    Canvas drawPoint(Point p) {
        points.add p
        return this
    }
}
