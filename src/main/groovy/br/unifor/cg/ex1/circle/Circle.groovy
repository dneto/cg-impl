package br.unifor.cg.ex1.circle

import br.unifor.cg.ex1.Point
import br.unifor.cg.ex1.Shape

trait Circle implements Shape{

    int radius
    Point origin

    protected List<Point> buffer = []

    abstract Point[] generateCirclePoints();

    void addPoint(Point point) {
        int x = point.x,
            y = point.y,
            orX = origin.x,
            orY = origin.y

        buffer.add([orX + x, orY + y] as Point)
        buffer.add([orX + x, orY - y] as Point)
        buffer.add([orX - x, orY + y] as Point)
        buffer.add([orX - x, orY - y] as Point)
        buffer.add([orY + y, orX + x] as Point)
        buffer.add([orY + y, orX - x] as Point)
        buffer.add([orY - y, orX + x] as Point)
        buffer.add([orY - y, orX - x] as Point)

    }

    Point[] buffer(){
        buffer;
    }

}