package br.unifor.cg.ex1.circle

import br.unifor.cg.ex1.Point

/**
 * Created by demetrio on 27/08/14.
 */
class CircleFactory {
    private CircleFactory(){}

    static Circle getCircle(CircleType circleType, int radius, Point origin){
        Circle circle;
        switch (circleType){
            case CircleType.MEDIUM_POINT:
                circle = [radius, origin] as CircleMP;
                break;
        }

        circle
    }

    static enum CircleType{
        MEDIUM_POINT
    }
}
