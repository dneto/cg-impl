package br.unifor.cg.ex1.circle

import br.unifor.cg.ex1.Point

class CircleMP implements Circle {

    @Override
    Point[] generateCirclePoints() {
        int x = 0,
            y = this.radius,
            d = 1 - this.radius;

        addPoint([x,y] as Point)

        while (y > x) {
            if (d < 0) {
                d = d + 2 * x + 3
            } else {
                d = d + 2 * (x - y) + 5
                y = y-1
            }
            x = x + 1
            addPoint([x, y] as Point)
        }

        buffer()
    }

    @Override
    Point[] getPoints() {
        generateCirclePoints()
    }
}
