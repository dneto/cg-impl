package br.unifor.cg.ex1.line

import br.unifor.cg.ex1.Point
import br.unifor.cg.ex1.Shape
import groovy.transform.ToString

/**
 * Created by demetrio on 13/08/14.
 */
@ToString
trait Line implements Shape{
    Point initialPoint, finalPoint
    double m, b
    boolean swapped

    Point[] getPoints() {
        this.m = finalPoint.x - initialPoint.x == 0 ? 0 : (finalPoint.y - initialPoint.y) / (finalPoint.x - initialPoint.x);
        this.b = initialPoint.y - (m * initialPoint.x)

        println "m = ${m} and b = ${b}"

        if (m <= -1 || m >= 1) {
            this.swapped = true;
            def iY = initialPoint.x
            def iX = initialPoint.y

            def fY = finalPoint.x
            def fX = finalPoint.y

            this.initialPoint = [iX, iY] as Point
            this.finalPoint = [fX, fY] as Point
        }

        return calculateLine()
    }


    abstract def calculateLine();
}


