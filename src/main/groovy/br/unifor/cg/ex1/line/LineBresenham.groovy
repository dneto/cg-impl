package br.unifor.cg.ex1.line

import br.unifor.cg.ex1.Point

/**
 * Created by demetrio on 19/08/14.
 */
class LineBresenham implements Line {

    def yDir = 1;
    def xDir = 1;

    LineBresenham(initialPoint, finalPoint){
        this.initialPoint = initialPoint as Point
        this.finalPoint = finalPoint as Point
    }


    @Override
    Point[] getPoints() {
        if(m < 0 && m > -1){
            yDir = -1;
        }

        if(initialPoint.x > finalPoint.x){
            yDir = -1;
            def aux = initialPoint;
            this.initialPoint = finalPoint;
            this.finalPoint = aux;

        }

        calculateLine()
    }

    @Override
    def calculateLine() {
        List<Point> pointList = new ArrayList<>()
        def dx = Math.abs(finalPoint.x - initialPoint.x)
        def dy = Math.abs(finalPoint.y - initialPoint.y)

        def d = 2 * dy - dx;
        def incE = 2 * dy;
        def incNE = 2 * (dy - dx);

        def x = initialPoint.x;
        def y = initialPoint.y;

        pointList.add(initialPoint)

        while (x < finalPoint.x) {
            if (d <= 0) {
                d = d + incE
            } else {
                d = d + incNE
                y += yDir
            }
            x += xDir

            pointList.add ([x,y] as Point);
        }

        return pointList;
    }
}
