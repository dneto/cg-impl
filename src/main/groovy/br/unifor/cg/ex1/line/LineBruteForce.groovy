package br.unifor.cg.ex1.line

import br.unifor.cg.ex1.Point


class LineBruteForce implements Line {

    def LineBruteForce(Point initialPoint, Point finalPoint) {
        super(initialPoint, finalPoint)
    }

    @Override
    def calculateLine() {
        def pointList = []

        (initialPoint.x..finalPoint.x).each { x ->
            def y = this.m * x + this.b;
            pointList.add swapped ? [y, x] : [x, y]
        }


        return pointList
    }
}
