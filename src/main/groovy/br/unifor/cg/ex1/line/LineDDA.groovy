package br.unifor.cg.ex1.line

import br.unifor.cg.ex1.Point
import groovy.util.logging.Log

/**
 * Created by demetrio on 13/08/14.
 */
@Log
class LineDDA implements Line {

    def LineDDA(Point initialPoint, Point finalPoint) {
        super(initialPoint, finalPoint)
    }

    @Override
    def calculateLine() {
        double m = this.m
        if(initialPoint.compareTo(finalPoint) == 1){
            m = m * -1
        }
        def pointList = []

        if (swapped) {
            def x = initialPoint.x

            (initialPoint.y..finalPoint.y).each{ y ->
                pointList.add new Point((int)Math.round(x), (int)Math.round(y))
                x += 1/m
            }
        } else {
            def y = initialPoint.y

            (initialPoint.x..finalPoint.x).each { x ->
                pointList.add new Point((int)Math.round(x), (int)Math.round(y))
                y += m
            }
        }

        return pointList
    }


}
