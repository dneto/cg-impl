package br.unifor.cg.ex1.line

import br.unifor.cg.ex1.Point

class LineFactory {
    private LineFactory() {}

    static Line getLine(LineType lineType, Point initialPoint, Point finalPoint) {
        Line line;
        switch (lineType) {
            case LineType.BRUTE_FORCE:
                line = [initialPoint, finalPoint] as LineBruteForce
                break
            case LineType.DDA:
                line = [initialPoint, finalPoint] as LineDDA
                break
            case LineType.BRESEHAM:
                line = [initialPoint, finalPoint] as LineBresenham
                break
        }
        line
    }

    static enum LineType {
        BRUTE_FORCE,
        DDA,
        BRESEHAM
    }
}
