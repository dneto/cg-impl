package br.unifor.cg.ex1.line

import br.unifor.cg.ex1.Point

/**
 * Created by demetrio on 14/08/14.
 */
class LineUtils {
    static def horizontalLine(y,x1,x2){
        def points = []

        (x1..x2).each {
            points.add new Point(it, x1);
        }

        return points;
    }

    static def verticalLine(x,y1,y2){
        def points = []

        (y1..y2).each {
            points.add new Point(x, it);
        }

        return points;
    }
}
