package br.unifor.cg.ex1.line

import br.unifor.cg.ex1.Point

import java.awt.Color

class LineXiaolinWu implements Line {

    LineXiaolinWu(initialPoint, finalPoint) {
        this.initialPoint = initialPoint as Point
        this.finalPoint = finalPoint as Point
    }

    @Override
    Point[] getPoints() {
        return calculateLine();
    }

    @Override
    def calculateLine() {
        List<Point> pointList = new ArrayList<Point>();

        int x0 = initialPoint.x;
        int x1 = finalPoint.x;

        int y0 = initialPoint.y
        int y1 = finalPoint.y

        boolean steep = Math.abs(y1 - y0) > Math.abs(x1 - x0)

        if (steep) {
            def aux = x0
            x0 = y0
            y0 = aux

            aux = x1
            x1 = y1;
            y1 = aux
        }

        if (x0 > x1) {
            def aux = x0
            x0 = x1
            x1 = aux

            aux = y0
            y0 = y1
            y1 = aux
        }

        int dx = x1 - x0;
        int dy = y1 - y0;

        double gradient = (dy as double) / (dx as double)

        double xEnd = round(x0);
        double yEnd = y0 + gradient * (xEnd - x0);

        double xGap = rfpart(x0 + 0.5);
        int xpx11 = xEnd
        int ypx11 = ipart(yEnd)

        if (steep) {
            pointList.add(new Point((ypx11), (xpx11), color(rfpart(yEnd) * xGap)));
            pointList.add(new Point((ypx11 + 1), (xpx11), color(fpart(yEnd) * xGap)));
        } else {
            pointList.add(new Point((xpx11), (ypx11), color(rfpart(yEnd) * xGap)));
            pointList.add(new Point((xpx11), (ypx11 + 1), color(fpart(yEnd) * xGap)));
        }

        double intery = yEnd + gradient;

        xEnd = round(x1)
        yEnd = y1 + gradient * (xEnd - x1)
        xGap = fpart(x1 + 0.5)
        int xpx12 = xEnd
        int ypx12 = ipart(yEnd)

        if (steep) {
            pointList.add(new Point((ypx12), (xpx12), color(rfpart(yEnd) * xGap)));
            pointList.add(new Point((ypx12 + 1), (xpx12), color(fpart(yEnd) * xGap)));
        } else {
            pointList.add(new Point((xpx12), (ypx12), color(rfpart(yEnd) * xGap)));
            pointList.add(new Point((xpx12), (ypx12 + 1), color(fpart(yEnd) * xGap)));
        }


        for (int x = xpx11 + 1; x < xpx12; x++) {
            if (steep) {
                pointList.add(new Point(ipart(intery), x, color(rfpart(intery))));
                pointList.add(new Point(ipart(intery) + 1, x, color(fpart(intery))));
                System.out.println(xpx11 + " " + xpx12)
            } else {
                pointList.add(new Point(x, ipart(intery), color(rfpart(intery))));
                pointList.add(new Point((x), ipart(intery) + 1, color(fpart(intery))));
            }

            intery = intery + gradient;
        }

        return pointList
    }

    private static int ipart(double x){
        //is 'integer part of x'
        return  (int)Math.floor(x);
    }

    private static int round(double x) {
        return ipart(x + 0.5);
    }

    private static double fpart(double x) {
        //is 'fractional part of x'
        return (double)Math.round((x - Math.floor(x))*100)/(double)100;
    }

    private static double rfpart(double x) {
        return 1 - fpart(x);
    }

    private static Color color(double n) {
        int color = 255
        color = (int) Math.round((double) color * n)
        color = 255 - color

        return new Color(0,0,0,color);
    }

}
